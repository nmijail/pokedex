/* eslint-disable global-require */
import React from 'react';
import { useFonts } from 'expo-font';
import AppLoading from 'expo-app-loading';
import { Provider } from 'react-redux';
import store from './src/application/modules';
import Navigation from './src/application/navigation';

export default function App() {
  const [fontsIsLoaded] = useFonts({
    LexendDeca: require('./assets/fonts/LexendDeca-Regular.ttf'),
  });

  if (!fontsIsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}
