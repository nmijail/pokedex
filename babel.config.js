module.exports = function (api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo', '@babel/preset-typescript'],
    plugins: [
      [
        require.resolve('babel-plugin-module-resolver'),
        {
          root: ['./src'],
          alias: {
            Component: './src/application/components',
            Screen: './src/application/screens',
            Modules: './src/application/modules',
            Business: './src/business',
            Infra: './src/infraestructure',
          },
          extensions: ['.ts', '.tsx'],
        },
      ],
      ['module:react-native-dotenv'],
    ],
  };
};
