import axios from 'axios';
import { evolve, identity, map } from 'ramda';
import { EXPO_POKEDEX_API, EXPO_POKEMON_IMAGE } from '@env';
import Pokemon from '~/business/pokemon/domain/pokemon';
import PokemonRepository from '~/business/pokemon/port/pokemonRepository';

const pokemonSchema = {
  id: identity,
  name: identity,
  height: identity,
  weight: identity,
};

interface PokemonDTO {
  id: number;
  name: string;
  height: number;
  weight: number;
}

interface PokemonListDTO {
  name: string;
  url: string;
}

const transformToPokemon = evolve(pokemonSchema);
const mapPokemons = map((item: PokemonDTO) => {
  const pokemon: Pokemon = {
    ...item,
    image: `${EXPO_POKEMON_IMAGE}/${item.id}.png`,
  };

  return pokemon;
});

export default class PokemonRepoImpl implements PokemonRepository {
  // eslint-disable-next-line class-methods-use-this
  async getAll(offset: number): Promise<Pokemon[]> {
    try {
      const result = await axios.get(`${EXPO_POKEDEX_API}/pokemon?offset=${offset}`);
      const pokemons: PokemonDTO[] = await Promise.all(
        result.data.results.map(async (item: PokemonListDTO) => {
          const { data } = await axios.get(item.url);
          const transformed = transformToPokemon(data);

          return transformed;
        }),
      );

      const pokemonList = {
        ...result.data,
        results: mapPokemons(pokemons),
      };
      return pokemonList;
    } catch (error) {
      throw new Error('error getting pokemon list');
    }
  }
}
