/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators, AnyAction } from 'redux';
import { getList, selectPokemon } from 'Modules/pokemon';
import { ApplicationState } from 'Modules';
import Pokedex from './Pokedex';
import Pokemon from '~/business/pokemon/domain/pokemon';

function PokedexContainer({ getAll, navigation, pokemons, onSelectPokemon, ...rest }) {
  const showPokemonDetail = (pokemon: Pokemon) => {
    onSelectPokemon(pokemon);
    navigation.navigate('Pokemon');
  };

  useEffect(() => {
    getAll();
  }, [getAll]);
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Pokedex pokemons={pokemons} {...rest} showPokemonDetail={showPokemonDetail} loadMore={getAll} />;
}

const mapStateToProps = ({ pokemon }: ApplicationState) => {
  return {
    pokemons: pokemon.pokemonList.results,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<ApplicationState, void, AnyAction>) =>
  bindActionCreators({ getAll: getList, onSelectPokemon: selectPokemon }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PokedexContainer);
