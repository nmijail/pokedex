/* eslint-disable global-require */
import React from 'react';
import { FlatList, ActivityIndicator } from 'react-native';
import Search from 'Component/Search';
import styled from 'styled-components/native';
import PokemonCard from 'Component/PokemonCard';
import Container from 'Component/Container';
import Row from 'Component/Row';
import Title from 'Component/Title';
import Pokemon from '~/business/pokemon/domain/pokemon';

const CardContainer = styled.View`
  border-radius: 50px;
  background-color: #353c63;
  width: 100px;
  margin: auto 35px auto 35px;
  height: 400px;
  display: flex;
  justify-content: center;
`;

export default function index({
  pokemons,
  loadMore,
  showPokemonDetail,
}: {
  pokemons: Pokemon[];
  loadMore(): void;
  // eslint-disable-next-line no-unused-vars
  showPokemonDetail(pokemon: Pokemon): void;
}) {
  const handleOnSelectPokemon = (pokemon: Pokemon) => {
    showPokemonDetail(pokemon);
  };

  const renderItem = ({ item }: { item: Pokemon }) => (
    <PokemonCard pokemon={item} onPress={() => handleOnSelectPokemon(item)} />
  );

  const endListComponent = () => (
    <CardContainer>
      <ActivityIndicator size="large" color="yellow" />
    </CardContainer>
  );

  const handleOnEndReached = () => {
    loadMore();
  };

  return (
    <Container>
      <Row style={{ width: '90%' }}>
        <Title>Pokedex</Title>
        <Search style={{ position: 'absolute', right: -110, top: -130 }} />
      </Row>
      <FlatList
        style={{ maxHeight: 700 }}
        horizontal
        onEndReached={handleOnEndReached}
        ListFooterComponent={endListComponent}
        data={pokemons}
        renderItem={renderItem}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
}
