/* eslint-disable global-require */
import React from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import styled from 'styled-components/native';
import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import Button from '../../components/Button';
import Title from '../../components/Title';
import SubTitle from '../../components/SubTitle';
import Row from '../../components/Row';
import Span from '../../components/Span';

const Container = styled.View`
  flex: 1;
  background-color: #2b3151;
`;

const InnerContainer = styled.View`
  display: flex;
  min-height: 100%;
`;

const MainImage = styled.Image`
  height: 300px;
  width: auto;
  justify-content: center;
  align-items: center;
`;

type RootParamList = {
  Home: undefined;
  Pokedex: undefined;
  Pokemon: undefined;
};
export default function index({ navigation }: BottomTabScreenProps<RootParamList, 'Home'>) {
  const handleOnPress = () => {
    navigation.navigate('Pokedex');
  };
  return (
    <Container>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <InnerContainer>
          <Row>
            <SubTitle>Bienvenido al</SubTitle>
            <Title>Pokedex</Title>
          </Row>
          <Row>
            <MainImage resizeMode="contain" source={require('../../../../assets/pokemonsprin.png')} />
          </Row>
          <Button onPress={handleOnPress}>Ingresar</Button>
          <Row>
            <Span style={{ textAlign: 'center' }}>nmijail.vargas@gmail.com</Span>
          </Row>
        </InnerContainer>
      </SafeAreaView>
    </Container>
  );
}
