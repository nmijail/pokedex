/* eslint-disable global-require */
import React from 'react';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import Container from 'Component/Container';
import Row from 'Component/Row';
import SubTitle from 'Component/SubTitle';
import Title from 'Component/Title';
import Pokemon from '~/business/pokemon/domain/pokemon';

interface PokemonComponentProp {
  pokemon: Pokemon;
}

const screen = Dimensions.get('screen');

const PokemonImage = styled.Image`
  width: ${() => `${(screen.width * 0.5).toFixed(2)}px`};
  height: 150px;
  margin: auto;
  align-self: center;
`;

const PokemonType = styled.Image`
  width: 30px;
  height: 30px;
`;
const DescriptionText = styled.Text`
  font-size: 24px;
  color: white;
  font-family: 'LexendDeca';
  text-align: justify;
`;

const PokemonName = styled.Text`
  font-family: 'LexendDeca';
  text-transform: uppercase;
  font-size: 55px;
  color: #555b73
  font-weight: 700;
  margin-top: -80px;
  margin-bottom: 40px;
  z-index: -1;
  text-align: center;
`;

const images = new Map(
  Object.entries({
    bug: require('../../../../assets/types/bug.png'),
    dark: require('../../../../assets/types/dark.png'),
    dragon: require('../../../../assets/types/dragon.png'),
    electric: require('../../../../assets/types/electric.png'),
    fairy: require('../../../../assets/types/fairy.png'),
    fighting: require('../../../../assets/types/fighting.png'),
    fire: require('../../../../assets/types/fire.png'),
    flying: require('../../../../assets/types/flying.png'),
    ghost: require('../../../../assets/types/ghost.png'),
    grass: require('../../../../assets/types/grass.png'),
    ground: require('../../../../assets/types/ground.png'),
    ice: require('../../../../assets/types/ice.png'),
    normal: require('../../../../assets/types/normal.png'),
    poison: require('../../../../assets/types/poison.png'),
    psychic: require('../../../../assets/types/psychic.png'),
    rock: require('../../../../assets/types/rock.png'),
    steel: require('../../../../assets/types/steel.png'),
    water: require('../../../../assets/types/water.png'),
  }),
);

const InfoContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const StatContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

const Column = styled.View`
  padding: 10px;
`;

const StatColumn = styled.View`
  padding: 10px;
  flex: 33.333%;
`;

const TitleLabel = styled.Text`
  color: #a5a9b9;
  font-weight: 700;
  padding: 5px;
  text-transform: capitalize;
  text-align: center;
`;

const ValueLabel = styled.Text`
  color: #f7ba14;
  font-weight: 700;
  padding: 5px;
  text-align: center;
  text-transform: capitalize;
`;

export default function PokemonComponent(prop: PokemonComponentProp) {
  const { image, name, types, description, stats, weight, height, abilities } = prop.pokemon;
  return (
    <Container>
      <Row>
        <PokemonImage resizeMode="contain" source={{ uri: image }} />
        <PokemonName>{name}</PokemonName>
        <Title style={{ textTransform: 'capitalize' }}>
          {name}
          {types.map((type) => (
            <PokemonType key={type.type.name} source={images.get(type.type.name)} />
          ))}
        </Title>
      </Row>
      <Row>
        <DescriptionText>{description?.substring(0, 80)}...</DescriptionText>
        <InfoContainer>
          <Column>
            <TitleLabel>Height</TitleLabel>
            <ValueLabel>{height}</ValueLabel>
          </Column>
          <Column>
            <TitleLabel>Width</TitleLabel>
            <ValueLabel>{weight}</ValueLabel>
          </Column>
        </InfoContainer>
      </Row>
      <Row>
        <SubTitle>Abilities</SubTitle>
        <InfoContainer>
          {abilities.map((ability) => (
            <Column key={ability.ability.name}>
              <ValueLabel>{ability.ability.name}</ValueLabel>
            </Column>
          ))}
        </InfoContainer>
      </Row>
      <Row style={{}}>
        <SubTitle>Base Stats</SubTitle>
        <StatContainer>
          {stats.map((stat) => (
            <StatColumn key={stat?.stat.name}>
              <TitleLabel>{stat?.stat.name}</TitleLabel>
              <ValueLabel>{stat.base_stat}</ValueLabel>
            </StatColumn>
          ))}
        </StatContainer>
      </Row>
    </Container>
  );
}
