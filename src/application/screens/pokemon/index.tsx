/* eslint-disable global-require */
import React from 'react';
import { connect } from 'react-redux';
import { ThunkDispatch } from 'redux-thunk';
import { bindActionCreators, AnyAction } from 'redux';
import { ApplicationState } from 'Modules';
import PokemonComponent from './Pokemon';
import Pokemon from '~/business/pokemon/domain/pokemon';

interface PokemonContainerProp {
  pokemon: Pokemon;
}

function PokemonContainer({ pokemon }: PokemonContainerProp) {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <PokemonComponent pokemon={pokemon} />;
}

const mapStateToProps = ({ pokemon }: ApplicationState) => {
  return {
    pokemon: pokemon.selectedPokemon,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<ApplicationState, void, AnyAction>) =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(PokemonContainer);
