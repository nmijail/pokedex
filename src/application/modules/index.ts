import thunkMiddleware from 'redux-thunk';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import pokemon, { PokemonState } from './pokemon';

export interface ApplicationState {
  pokemonState: PokemonState;
}

const rootReducer = combineReducers({
  pokemon,
});

export default createStore(rootReducer, composeWithDevTools(applyMiddleware(thunkMiddleware)));
