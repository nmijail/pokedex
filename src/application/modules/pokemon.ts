import { fromPairs, pipe, tail, split, map, head } from 'ramda';
import axios from 'axios';
import Pokemon from '~/business/pokemon/domain/pokemon';
import GetPokemonListService from '../../business/pokemon/service/getPokemonListService';
import PokemonRepoImpl from '../../infrastructure/PokemonRepoImpl';

export const GET_ALL = 'POKEMON/GET_ALL';
export const SELECT_POKEMON = 'POKEMON/SELECT_POKEMON';

export interface PokemonState {
  pokemonList: {
    results: Array<Pokemon>;
  };
  selectedPokemon: Pokemon | undefined;
}

const initialState: PokemonState = {
  pokemonList: {
    results: [],
  },
  selectedPokemon: undefined,
};

const getQueryParams = pipe(split('?'), tail, head, split('&'), map(split('=')), fromPairs, map(decodeURI));

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ALL:
    case SELECT_POKEMON:
      return { ...state, ...payload };

    default:
      return state;
  }
};

export const getList = () => async (dispatch, getState) => {
  const { pokemon } = getState();
  const pokemonRepository = new PokemonRepoImpl();
  const getPokemonListService = new GetPokemonListService(pokemonRepository);
  const queryParams = pokemon.pokemonList.next ? getQueryParams(pokemon.pokemonList.next) : 20;

  try {
    const pokemonList = await getPokemonListService.execute(queryParams.offset);
    dispatch({
      type: GET_ALL,
      payload: {
        pokemonList: {
          ...pokemonList,
          results: [...pokemon.pokemonList.results, ...pokemonList.results],
        },
      },
    });
  } catch (error) {
    dispatch({
      type: GET_ALL,
      payload: { errorFetchingList: true },
    });
  }
};

export const selectPokemon = (selectedPokemon: Pokemon) => async (dispatch) => {
  dispatch({
    type: SELECT_POKEMON,
    payload: {
      selectedPokemon,
    },
  });
  try {
    const ability = head(selectedPokemon.abilities);
    const { data } = await axios.get(ability.ability.url);
    dispatch({
      type: SELECT_POKEMON,
      payload: {
        selectedPokemon: {
          ...selectedPokemon,
          description: data.effect_entries[0].effect,
        },
      },
    });
  } catch (error) {
    throw new Error('failing getting ability');
  }
};
