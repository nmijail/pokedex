/* eslint-disable global-require */
import React, { PropsWithChildren } from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import styled from 'styled-components/native';

const Container = styled.View`
  flex: 1;
  background-color: #2b3151;
`;

const InnerContainer = styled.View`
  display: flex;
  min-height: 100%;
`;

export default function index({ children }: PropsWithChildren<React.ReactElement>) {
  return (
    <Container>
      <StatusBar barStyle="light-content" />
      <SafeAreaView>
        <InnerContainer>{children}</InnerContainer>
      </SafeAreaView>
    </Container>
  );
}
