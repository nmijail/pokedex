/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import React, { useState } from 'react';
import { ActivityIndicator, Dimensions } from 'react-native';
import styled from 'styled-components/native';
import Pokemon from '~/business/pokemon/domain/pokemon';
import Title from './Title';

const screen = Dimensions.get('screen');

const CardContainer = styled.TouchableOpacity`
  border-radius: 50px;
  background-color: #353c63;
  width: ${() => `${(screen.width * 0.8).toFixed(2)}px`};
  margin: auto 35px auto 35px;
  height: 400px;
  display: flex;
`;

const TitleLabel = styled.Text`
  color: #a5a9b9;
  font-weight: 700;
  padding: 5px;
  text-transform: capitalize;
`;

const ValueLabel = styled.Text`
  color: #f7ba14;
  font-weight: 700;
  padding: 5px;
  text-align: center;
`;

const PokemonImage = styled.Image`
  width: ${() => `${(screen.width * 0.5).toFixed(2)}px`};
  height: 300px;
  position: absolute;
  top: -150px;
  margin: auto;
  align-self: center;
`;

const BasicsContainer = styled.View`
  display: flex;
  margin-top: 30%;
  padding: 12px 20px 12px 20px;
`;

const InfoContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
`;

const Column = styled.View`
  padding: 10px;
`;

const PokemonType = styled.Image`
  width: 30px;
  height: 30px;
`;

const ImageLoaderIndicatorContainer = styled.View`
  width: ${() => `${(screen.width * 0.5).toFixed(2)}px`};
  min-height: 300px;
  position: absolute;
  top: -150px;
  margin: auto;
  align-self: center;
  background: ;
  border-radius: 50px;
  align-items: center;
  justify-content: center;
  display: flex;
`;

interface PokemonCardProps {
  pokemon: Pokemon;
  onPress(): void;
}

export default function PokemonCard({ pokemon, onPress }: PokemonCardProps) {
  const [isLoading, setIsLoading] = useState(false);
  const handleOnLoadStart = () => {
    setIsLoading(true);
  };

  const handleOnLoadEnd = () => {
    setIsLoading(false);
  };

  const images = new Map(
    Object.entries({
      bug: require('../../../assets/types/bug.png'),
      dark: require('../../../assets/types/dark.png'),
      dragon: require('../../../assets/types/dragon.png'),
      electric: require('../../../assets/types/electric.png'),
      fairy: require('../../../assets/types/fairy.png'),
      fighting: require('../../../assets/types/fighting.png'),
      fire: require('../../../assets/types/fire.png'),
      flying: require('../../../assets/types/flying.png'),
      ghost: require('../../../assets/types/ghost.png'),
      grass: require('../../../assets/types/grass.png'),
      ground: require('../../../assets/types/ground.png'),
      ice: require('../../../assets/types/ice.png'),
      normal: require('../../../assets/types/normal.png'),
      poison: require('../../../assets/types/poison.png'),
      psychic: require('../../../assets/types/psychic.png'),
      rock: require('../../../assets/types/rock.png'),
      steel: require('../../../assets/types/steel.png'),
      water: require('../../../assets/types/water.png'),
    }),
  );

  return (
    <CardContainer onPress={onPress}>
      {isLoading && (
        <ImageLoaderIndicatorContainer>
          <ActivityIndicator color="yellow" size="large" />
        </ImageLoaderIndicatorContainer>
      )}
      <PokemonImage
        onLoadStart={handleOnLoadStart}
        onLoadEnd={handleOnLoadEnd}
        key={pokemon.id}
        resizeMode="contain"
        source={{ uri: pokemon.image }}
      />
      <BasicsContainer>
        <Title color="white" style={{ textTransform: 'capitalize' }}>
          {pokemon.name}
          {pokemon.types.map((type) => (
            <PokemonType key={type.type.name} source={images.get(type.type.name)} />
          ))}
        </Title>
        <InfoContainer>
          <Column>
            <TitleLabel>Height</TitleLabel>
            <ValueLabel>{pokemon.height}</ValueLabel>
          </Column>
          <Column>
            <TitleLabel>Width</TitleLabel>
            <ValueLabel>{pokemon.weight}</ValueLabel>
          </Column>
        </InfoContainer>
        <InfoContainer>
          {pokemon.stats
            .filter((stat) => ['hp', 'attack', 'defense'].includes(stat.stat.name))
            .map((stat) => (
              <Column key={stat?.stat.name}>
                <TitleLabel>{stat?.stat.name}</TitleLabel>
                <ValueLabel>{stat.base_stat}</ValueLabel>
              </Column>
            ))}
        </InfoContainer>
      </BasicsContainer>
    </CardContainer>
  );
}
