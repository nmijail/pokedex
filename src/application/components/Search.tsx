/* eslint-disable global-require */
import React from 'react';
import { View, TextInput, TouchableOpacity, StyleProp } from 'react-native';
import LottieView from 'lottie-react-native';

interface State {
  isOpen: boolean;
  frame: number;
}

interface Props {
  style: StyleProp<View>;
}
export default class App extends React.Component<Props, State> {
  animation: any;

  text: any;

  constructor(props: Props) {
    super(props);

    this.state = {
      isOpen: false,
      frame: 120,
    };
  }

  componentDidMount() {
    this.animation.play(120, 120);
    this.setState({
      isOpen: false,
    });
  }

  resetAnimation = () => {
    this.animation.reset();
    this.animation.play();
  };

  handleOnPress = () => {
    this.animation.reset();
    const { frame } = this.state;
    if (frame === 180) {
      this.setState({
        isOpen: false,
        frame: 120,
      });
      this.animation.play(32, 120);
    } else if (frame === 120) {
      this.setState(
        {
          isOpen: true,
          frame: 180,
        },
        () => {
          this.text.focus();
        },
      );
      this.animation.play(121, 180);
    }
  };

  render() {
    const { style } = this.props;
    const { isOpen } = this.state;
    return (
      <View style={style}>
        <TouchableOpacity style={{ width: 300, height: 100, backgroundColor: 'white' }} onPress={this.handleOnPress}>
          <LottieView
            autoSize
            loop={false}
            autoPlay={false}
            ref={(animation) => {
              this.animation = animation;
            }}
            style={{
              width: 300,
              backgroundColor: '#2b3151',
            }}
            resizeMode="contain"
            source={require('../../../assets/search.json')}
          />
        </TouchableOpacity>
        {isOpen && (
          <TextInput
            ref={(text) => {
              this.text = text;
            }}
            style={{
              position: 'absolute',
              height: 70,
              width: 170,
              paddingRight: 0,
              paddingLeft: 90,
              zIndex: 2,
              top: 115,
              color: 'white',
              fontSize: 20,
            }}
          />
        )}
      </View>
    );
  }
}
