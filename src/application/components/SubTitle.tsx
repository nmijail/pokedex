import styled from 'styled-components/native';

const SubTitle = styled.Text`
  color: white;
  font-family: 'LexendDeca';
  font-weight: 400;
`;

export default SubTitle;
