import styled from 'styled-components/native';

const Text = styled.Text<{ medium: boolean }>`
  font-family: 'LexendDeca';
  font-size: ${(props) => (props.medium ? '24px' : '36px')};
  color: ${({ color = '#f7ba14' }) => color};
  font-weight: 600;
`;

export default Text;
