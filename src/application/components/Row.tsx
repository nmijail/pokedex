import styled from 'styled-components/native';

const Row = styled.View`
  width: 80%;
  margin: auto;
`;

export default Row;
