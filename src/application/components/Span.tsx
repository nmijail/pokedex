import styled from 'styled-components/native';

const Span = styled.Text`
  font-size: 16px;
  color: gray;
  font-weight: normal;
`;

export default Span;
