import React from 'react';
import styled from 'styled-components/native';

const Button = styled.View`
  flex-grow: 1;
  align-items: center;
`;

const IconContainer = styled.View<{ isActive: boolean }>`
  border-radius: 50px;
  background-color: ${(props) => (props.isActive ? '#252c4c' : 'transparent')};
  height: 46px;
  width: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default function BarButton({
  color,
  children,
}: {
  color: string;
  children: React.ReactElement;
}): React.ReactElement {
  return (
    <Button>
      <IconContainer isActive={color === '#f7ba14'}>{children}</IconContainer>
    </Button>
  );
}
