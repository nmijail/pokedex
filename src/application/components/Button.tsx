/* eslint-disable no-unused-vars */
import React from 'react';
import styled from 'styled-components/native';

interface ButtonProps {
  children: string;
  onPress(): void;
}

const MainButton = styled.TouchableOpacity`
  background: #f7ba14;
  width: 80%;
  margin: auto;
  min-height: 40px;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ButtonText = styled.Text`
  font-family: 'LexendDeca';
  font-weight: 600;
  font-size: 16px;
  color: darkslategray;
`;

export default function Button({ children, onPress }: ButtonProps) {
  return (
    <MainButton onPress={onPress}>
      <ButtonText onPress={onPress}>{children}</ButtonText>
    </MainButton>
  );
}
