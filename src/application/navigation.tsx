import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MainScreen from 'Screen/main';
import PokemonScreen from 'Screen/pokemon';
import PokedexScreen from 'Screen/pokedex';
import BarButton from './components/BarButton';

const Bottom = createBottomTabNavigator();
const styles = {
  backgroundColor: '#363d64',
  borderTopColor: '#363d64',
  height: 60,
};

export default function navigation() {
  return (
    <NavigationContainer>
      <Bottom.Navigator tabBarOptions={{ style: { ...styles }, showLabel: false, activeTintColor: '#f7ba14' }}>
        <Bottom.Screen
          name="Home"
          component={MainScreen}
          options={() => ({
            tabBarVisible: false,
            tabBarIcon: ({ color }: any) => {
              return (
                <BarButton color={color}>
                  <MaterialCommunityIcons name="home" size={40} color={color} />
                </BarButton>
              );
            },
          })}
        />
        <Bottom.Screen
          name="Pokedex"
          component={PokedexScreen}
          options={{
            tabBarIcon: ({ color }: any) => {
              return (
                <BarButton color={color}>
                  <MaterialCommunityIcons name="pokeball" size={40} color={color} />
                </BarButton>
              );
            },
          }}
        />
        <Bottom.Screen
          name="Pokemon"
          component={PokemonScreen}
          options={{
            tabBarIcon: ({ color }: any) => {
              return (
                <BarButton color={color}>
                  <MaterialCommunityIcons name="pokemon-go" size={40} color={color} />
                </BarButton>
              );
            },
          }}
        />
      </Bottom.Navigator>
    </NavigationContainer>
  );
}
