import Pokemon from '../domain/pokemon';

export default interface PokemonRepository {
  // eslint-disable-next-line no-unused-vars
  getAll(offset: number): Promise<Array<Pokemon>>;
}
