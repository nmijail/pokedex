export default class Pokemon {
  id: number;

  name: string;

  height: number;

  weight: number;

  image: string;

  stats: any;

  types: any;

  description: string;

  abilities: Array<{ ability: { name: string } }>;
}
