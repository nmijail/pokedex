import Pokemon from '../domain/pokemon';

export default interface GetPokemonListUseCase {
  // eslint-disable-next-line no-unused-vars
  execute(offset: number): Promise<Pokemon[]>;
}
