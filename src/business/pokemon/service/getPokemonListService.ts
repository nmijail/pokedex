import Pokemon from '../domain/pokemon';
import PokemonRepository from '../port/pokemonRepository';
import GetPokemonListUseCase from '../useCase/getPokemonListUseCase';

export default class GetPokemonListService implements GetPokemonListUseCase {
  repository: PokemonRepository;

  constructor(repository: PokemonRepository) {
    this.repository = repository;
  }

  execute(offset: number): Promise<Pokemon[]> {
    return this.repository.getAll(offset);
  }
}
